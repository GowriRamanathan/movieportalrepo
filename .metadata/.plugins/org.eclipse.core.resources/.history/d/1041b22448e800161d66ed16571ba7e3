
package com.movie.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.movie.domain.Movie;
import com.movie.domain.MoviePortalUser;
import com.movie.domain.Payment;

@Repository
public class RegularUserMoviePoratlDao implements IMoviePortalDao1 {

	public static final int DISCOUNT_RATE = 20;

	JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Transactional
	public void rentMovie(MoviePortalUser user) {
		int userId = insertMoviePortalUser(user);
		updateMovie(user.getMovieList());
		insertUserMovieMapping(userId, user.getMovieList());
	}

	public int insertMoviePortalUser(MoviePortalUser user) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		String userSql = "INSERT INTO movie_portal_user(name, password, email_address, user_type, discount_rate) values (?,?,?,?)";
		jdbcTemplate.update(userSql, user.getName(), user.getPassword(), user.getEmailAddress(), user.getUserType(),
				user.getDiscountRate(), keyHolder);
		return keyHolder.getKey().intValue();
	}

	public void updateMovie(final List<Movie> movieList) {

		List<Integer> inputList = new ArrayList<Integer>();
		for (Movie m : movieList) {
			inputList.add(m.getMovieId());
		}
		final LocalDate date = LocalDate.now().plus(3, ChronoUnit.DAYS);
		String movieSql = "Update movie SET is_rented = true , expiry_date = ?, rental_amount = ? WHERE movie_id = ?";
		jdbcTemplate.batchUpdate(movieSql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Movie m = movieList.get(i);
				ps.setInt(1, m.getMovieId());
				ps.setDate(2, java.sql.Date.valueOf(date));
				ps.setFloat(3, m.getRentalAmount());
			}
			public int getBatchSize() {
				return movieList.size();
			}
		});
	}

	public void insertUserMovieMapping(final int userId, final List<Movie> movieList) {
		String userMovieSql = "INSERT INTO movie_user_mapping(user_id, movie_id) values (?,?) ";
		jdbcTemplate.batchUpdate(userMovieSql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ps.setInt(1, userId);
				Movie m = movieList.get(i);
				ps.setInt(2, m.getMovieId());
			}

			public int getBatchSize() {
				return movieList.size();
			}
		});
	}

	public void insertUserPayment(Payment p, int userId) {
		String userPaymentSql = "INSERT INTO user_payment(user_id, card_number, bank_name, card_type, ifsc_code) ";
		jdbcTemplate.update(userPaymentSql, userId, p.getCardNumber(), p.getBankName(), p.getCardType(),
				p.getIFSCCode());
	}

}
