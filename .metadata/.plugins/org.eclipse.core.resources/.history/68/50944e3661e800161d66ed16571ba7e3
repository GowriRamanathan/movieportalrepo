
package com.movie.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.movie.constants.DBConstants;
import com.movie.domain.Movie;
import com.movie.domain.MoviePortalUser;
import com.movie.domain.Payment;

@Repository
@Qualifier("Regular")
public class RegularUserMoviePortalDao implements IMoviePortalDao1 {

	public static final int DISCOUNT_RATE = 20;

	JdbcTemplate jdbcTemplate;	

	public RegularUserMoviePortalDao(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

	@Transactional
	public void rentMovie(MoviePortalUser user) {
		int userId = insertMoviePortalUser(user);
		updateMovie(user.getMovieList());
		insertUserMovieMapping(userId, user.getMovieList());
	}

	public int insertMoviePortalUser(MoviePortalUser user) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(DBConstants.INSERT_MOVIE_USER_SQL, user.getName(), user.getPassword(), user.getEmailAddress(), user.getUserType(),
			 keyHolder);
		return keyHolder.getKey().intValue();
	}

	public void updateMovie(final List<Movie> movieList) {

		List<Integer> inputList = new ArrayList<Integer>();
		for (Movie m : movieList) {
			inputList.add(m.getMovieId());
		}
		final LocalDate date = LocalDate.now().plus(3, ChronoUnit.DAYS);
		jdbcTemplate.batchUpdate(DBConstants.UPDATE_MOVIE_USER_SQL, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Movie m = movieList.get(i);
				ps.setInt(1, m.getMovieId());
				ps.setDate(2, java.sql.Date.valueOf(date));
				ps.setFloat(3, m.getRentalAmount());
			}
			public int getBatchSize() {
				return movieList.size();
			}
		});
	}

	public void insertUserMovieMapping(final int userId, final List<Movie> movieList) {
		String userMovieSql = "INSERT INTO movie_user_mapping(user_id, movie_id) values (?,?) ";
		jdbcTemplate.batchUpdate(userMovieSql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ps.setInt(1, userId);
				Movie m = movieList.get(i);
				ps.setInt(2, m.getMovieId());
			}
			public int getBatchSize() {
				return movieList.size();
			}
		});
	}

}
