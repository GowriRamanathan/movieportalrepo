package com.movie.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.movie.constants.DBConstants;
import com.movie.domain.Movie;
import com.movie.domain.MoviePortalUser;

@Repository
public class SearchMoviePortalDao implements ISearchMoviePortalDao {
	JdbcTemplate jdbcTemplate;

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public SearchMoviePortalDao(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public List<Movie> searchMovieByGenere(String searchString) {

		List<Movie> movieList = jdbcTemplate.query(DBConstants.SELECT_MOVIES_BY_GENERE, new Object[] { searchString },
				new BeanPropertyRowMapper<Movie>(Movie.class));
		return movieList;
	}

	public List<Movie> searchMovieByYear(String searchString) {

		List<Movie> movieList = jdbcTemplate.query(DBConstants.SELECT_MOVIES_BY_YEAR, new Object[] { searchString },
				new BeanPropertyRowMapper<Movie>(Movie.class));
		return movieList;
	}

	public List<Movie> getRentedMovieList() {
		List<Movie> movieList = jdbcTemplate.query(DBConstants.SELECT_RENTED_MOVIES,
				new BeanPropertyRowMapper<Movie>(Movie.class));
		return movieList;
	}

	public String selectMoviePortalUser(MoviePortalUser user) {
		return jdbcTemplate.queryForObject(DBConstants.SELECT_MOVIE_USER_SQL, new Object[] { user.getEmailAddress() },
				String.class);
	}
}
