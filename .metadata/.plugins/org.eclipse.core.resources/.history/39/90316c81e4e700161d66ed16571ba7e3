package com.movie.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.movie.domain.Movie;
import com.movie.domain.MoviePortalUser;
import com.movie.domain.Payment;

@Repository
public class MoviePortalDaoController {

	JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public List<Movie> searchByGenre(String searchString, String columnName) {
		String sql = "select movie_id, movie_name, rental_amount, is_rented, genere, year_of_release from movie where ? = ?";
		List<Movie> movieList = jdbcTemplate.query(sql, new Object[] { columnName, searchString },
				new BeanPropertyRowMapper<Movie>(Movie.class));
		return movieList;
	}

	@Transactional
	public void rentMovie(MoviePortalUser user) {
		int userId = insertMoviePortalUser(user);
		updateMovie(user.getMovieList());
		insertUserMovieMapping(userId, user.getMovieList());
		insertUserPayment(user.getPayment(), userId);		
	}

	public int insertMoviePortalUser(MoviePortalUser user) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		String userSql = "INSERT INTO movie_portal_user(name, password, email_address, user_type, discount_rate) values (?,?,?,?)";
		jdbcTemplate.update(userSql, user.getName(), user.getPassword(), user.getEmailAddress(), user.getUserType(),
				user.getDiscountRate(),keyHolder);
		return keyHolder.getKey().intValue();
	}

	public void updateMovie(final List<Movie> movieList) {
		List<Integer> inputList = new ArrayList<Integer>();
		for (Movie m : movieList) {
			inputList.add(m.getMovieId());
		}
		String movieSql = "Update movie SET is_rented = true AND expiry_date = SYSDATE WHERE movie_id = ?";
		jdbcTemplate.batchUpdate(movieSql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Movie m = movieList.get(i);
				ps.setInt(1, m.getMovieId());

			}
			public int getBatchSize() {
				return movieList.size();
			}
		});
	}
	
	public void insertUserMovieMapping(final int userId, final List<Movie> movieList){
		String userMovieSql = "INSERT INTO movie_user_mapping(user_id, movie_id) values (?,?) ";
		jdbcTemplate.batchUpdate(userMovieSql, new BatchPreparedStatementSetter() {			
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ps.setInt(1, userId);
				Movie m = movieList.get(i);
				ps.setInt(2, m.getMovieId());				
			}		
			public int getBatchSize() {
				return movieList.size();
			}
		});			
	}
	
	public void insertUserPayment(Payment p, int userId){
		String userPaymentSql = "INSERT INTO user_payment(user_id, card_number, bank_name, card_type, ifsc_code) ";
		jdbcTemplate.update(userPaymentSql,userId, p.getCardNumber(), p.getBankName(), p.getCardType(),p.getIFSCCode());
	}

	public List<Movie> getRentedMovieList() {
		List<Movie> movieList = this.jdbcTemplate.query(
				"select movie_id, movie_name, expiry_date from movie where is_Rented = true", 
				new BeanPropertyRowMapper<Movie>(Movie.class)
				);
		return movieList;
	}
}
