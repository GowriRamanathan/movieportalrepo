package com.movie.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.movie.domain.ErrorDetail;
import com.movie.domain.Movie;
import com.movie.domain.MoviePortalUser;
import com.movie.exceptionhandler.YearValidationException;
import com.movie.service.OnlineMoviePortalService;

@RestController
@EnableAutoConfiguration
@ComponentScan({ "com.movie.dao", "com.movie.service" })
public class OnlineMovieController {

	@Autowired
	OnlineMoviePortalService onlineMoviePortalService;

	@RequestMapping(value = "/movie/genre/{genre}", method = RequestMethod.GET)
	@ResponseBody
	List<Movie> getMovieByGenre(@PathVariable String genre) {
		List<Movie> movieList = onlineMoviePortalService.getMovieListByGenre(genre);
		return movieList;
	}

	@RequestMapping(value = "/movie/year", method = RequestMethod.GET)
	@ResponseBody
	List<Movie> getMovieByYear(@RequestParam("year") String year) throws ConstraintViolationException {
		if (year.length() != 4) {
			throw new YearValidationException(year);
		} else {
			List<Movie> movieList = onlineMoviePortalService.getMovieListByYear(year);
			return movieList;
		}
	}

	@RequestMapping(value = "/movie/rent", method = RequestMethod.POST)
	@ResponseBody
	void rentMovie(@RequestBody @Valid MoviePortalUser user) {
		onlineMoviePortalService.rentMovie(user);
	}

	@RequestMapping(value = "/movie/rented", method = RequestMethod.GET)
	@ResponseBody
	List<Movie> getRentedMovies() {
		return onlineMoviePortalService.getRentedMoviesList();
	}

	@ExceptionHandler(value = { YearValidationException.class })
	public ErrorDetail myError(HttpServletRequest request, Exception exception) {
		ErrorDetail error = new ErrorDetail();
		error.setStatus(HttpStatus.BAD_REQUEST.value());
		error.setMessage(exception.getLocalizedMessage());
		error.setUrl(request.getRequestURL().toString());
		return error;
	}

	public static void main(String args[]) {
		SpringApplication.run(OnlineMovieController.class, args);
	}
}
