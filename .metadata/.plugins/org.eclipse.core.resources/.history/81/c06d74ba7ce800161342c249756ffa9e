package com.movie.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.movie.domain.Movie;
import com.movie.domain.MoviePortalUser;
import com.movie.service.OnlineMoviePortalService;

@RestController
@EnableAutoConfiguration
@ComponentScan({"com.movie.dao","com.movie.service"})
public class OnlineMovieController {
	
	@Autowired
	OnlineMoviePortalService onlineMoviePortalService;

	@RequestMapping(value = "/movie/genre/{genre}", method = RequestMethod.GET)
	@ResponseBody
	List<Movie> getMovieByGenre(@PathVariable String genre){
		List<Movie> movieList = onlineMoviePortalService.getMovieListByGenre(genre);
		return movieList;
	}
	
	@RequestMapping(value = "/movie/year/{year}", method = RequestMethod.GET)
	@ResponseBody
	List<Movie> getMovieByYear(@PathVariable String year){
		List<Movie> movieList = onlineMoviePortalService.getMovieListByYear(year);
		return movieList;
	}
	
	@RequestMapping(value = "/movie/rent", method = RequestMethod.POST)
	@ResponseBody
	void rentMovie(@RequestBody @Valid MoviePortalUser user){
		onlineMoviePortalService.rentMovie(user);
	}
	
	@RequestMapping(value = "/movie/rented", method = RequestMethod.GET)
	@ResponseBody
	List<Movie> getRentedMovies(){
		return onlineMoviePortalService.getRentedMoviesList();
	}
	
	public static void main(String args[]){
		SpringApplication.run(OnlineMovieController.class, args);
	}
}
