package com.movie.dao;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.movie.domain.Movie;

public class SearchMoviePortalDaoTest {
	SearchMoviePortalDao searchMoviePortalDao;
	JdbcTemplate jdbcTemplate;

	@Before
	public void setUp() throws Exception {
		DataSource datasource = (DataSource) mock(DataSource.class);
		searchMoviePortalDao = new SearchMoviePortalDao(datasource);
		jdbcTemplate = mock(JdbcTemplate.class);
		searchMoviePortalDao.setJdbcTemplate(jdbcTemplate);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void searchMovieByGenere() {
		String genere = "ROMANCE";
		List<Movie> movieList = new ArrayList<Movie>();
		Movie expectedMovie = new Movie();
		expectedMovie.setGenere(genere);
		expectedMovie.setMovieId(3);
		expectedMovie.setMovieName("TestMovieName");
		expectedMovie.setRented(true);
		expectedMovie.setYearOfRelease(2016);
		expectedMovie.setRentalAmount(2500F);
		movieList.add(expectedMovie);
		when(jdbcTemplate.query(any(PreparedStatementCreator.class), any(PreparedStatementSetter.class),
				any(ResultSetExtractor.class))).thenReturn(movieList);
		List<Movie> finalMovieList = searchMoviePortalDao.searchMovieByGenere(genere);
		assertNotNull(finalMovieList);

	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void searchMovieByYear() {
		String year = "2016";
		List<Movie> movieList = new ArrayList<Movie>();
		Movie expectedMovie = new Movie();
		expectedMovie.setGenere("TestGenre");
		expectedMovie.setMovieId(3);
		expectedMovie.setMovieName("TestMovieName");
		expectedMovie.setRented(true);
		expectedMovie.setYearOfRelease(2016);
		expectedMovie.setRentalAmount(2500F);
		movieList.add(expectedMovie);
		when(jdbcTemplate.query(any(PreparedStatementCreator.class), any(PreparedStatementSetter.class),
				any(ResultSetExtractor.class))).thenReturn(movieList);
		List<Movie> finalMovieList = searchMoviePortalDao.searchMovieByYear(year);
		assertNotNull(finalMovieList);

	}
}
