package com.movie.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import com.movie.dao.RegisteredUserMoviePortalDao;
import com.movie.dao.RegularUserMoviePortalDao;
import com.movie.dao.SearchMoviePortalDao;
import com.movie.domain.Movie;
import com.movie.domain.MoviePortalUser;

public class OnlineMovieServiceUnitTest {

	OnlineMoviePortalService onlineMoviePortalService;
	RegisteredUserMoviePortalDao registeredUserMoviePortalDao;
	RegularUserMoviePortalDao regularUserMoviePortalDao;
	SearchMoviePortalDao searchMoviePoratlDao;

	@Before
	public void setUp() {
		onlineMoviePortalService = new OnlineMoviePortalService();
		registeredUserMoviePortalDao = mock(RegisteredUserMoviePortalDao.class);
		regularUserMoviePortalDao = mock(RegularUserMoviePortalDao.class);
		searchMoviePoratlDao = mock(SearchMoviePortalDao.class);
		onlineMoviePortalService.setiSearchMoviePortalDao(searchMoviePoratlDao);
		onlineMoviePortalService.setRegisteredUserMoviePortalDao(registeredUserMoviePortalDao);
		onlineMoviePortalService.setRegularUserMoviePortalDao(regularUserMoviePortalDao);
	}

	@Test
	public void testGetMovieListByGenre() {
		String genere = "TestGenere";
		List<Movie> movieList = new ArrayList<Movie>();
		Movie expectedMovie = new Movie();
		expectedMovie.setGenere(genere);
		expectedMovie.setMovieId(3);
		expectedMovie.setMovieName("TestMovieName");
		expectedMovie.setRented(true);
		expectedMovie.setYearOfRelease(2016);
		expectedMovie.setRentalAmount(2500F);
		movieList.add(expectedMovie);
		when(onlineMoviePortalService.getMovieListByGenre(genere)).thenReturn(movieList);
		when(searchMoviePoratlDao.searchMovieByGenere(genere)).thenReturn(movieList);
		List<Movie> found = onlineMoviePortalService.getMovieListByGenre(genere);
		assertNotNull(found);
		assertEquals(movieList, found);
	}
	
	@Test
	public void testGetMovieListByYear() {
		String year = "2016";
		List<Movie> movieList = new ArrayList<Movie>();
		Movie expectedMovie = new Movie();
		expectedMovie.setGenere("TestGenere");
		expectedMovie.setMovieId(3);
		expectedMovie.setMovieName("TestMovieName");
		expectedMovie.setRented(true);
		expectedMovie.setYearOfRelease(2016);
		expectedMovie.setRentalAmount(2500F);
		movieList.add(expectedMovie);
		when(onlineMoviePortalService.getMovieListByYear(year)).thenReturn(movieList);
		when(searchMoviePoratlDao.searchMovieByYear(year)).thenReturn(movieList);
		List<Movie> found = onlineMoviePortalService.getMovieListByYear(year);
		assertNotNull(found);
		assertEquals(movieList, found);
	}
	
	@Test
	public void testRentedMovieList() {
		List<Movie> movieList = new ArrayList<Movie>();
		Movie expectedMovie = new Movie();
		expectedMovie.setGenere("TestGenere1");
		expectedMovie.setMovieId(3);
		expectedMovie.setMovieName("TestMovieName1");
		expectedMovie.setRented(true);
		expectedMovie.setYearOfRelease(2016);
		expectedMovie.setRentalAmount(2500F);
		movieList.add(expectedMovie);
		Movie expectedMovie2 = new Movie();
		expectedMovie2.setGenere("TestGenere2");
		expectedMovie2.setMovieId(4);
		expectedMovie2.setMovieName("TestMovieName2");
		expectedMovie2.setRented(true);
		expectedMovie2.setYearOfRelease(2016);
		expectedMovie2.setRentalAmount(2500F);
		movieList.add(expectedMovie2);
		when(onlineMoviePortalService.getRentedMoviesList()).thenReturn(movieList);
		when(searchMoviePoratlDao.getRentedMovieList()).thenReturn(movieList);
		List<Movie> found = onlineMoviePortalService.getRentedMoviesList();
		assertNotNull(found);
		assertEquals(movieList, found);
	}
	
	@Test
	public void testRentMovie() {
		MoviePortalUser moviePortalUser = new MoviePortalUser();
		moviePortalUser.setEmailAddress("testmail@test.com");
		List<Movie> movieList = new ArrayList<Movie>();
		Movie expectedMovie = new Movie();
		expectedMovie.setGenere("TestGenere1");
		expectedMovie.setMovieId(3);
		expectedMovie.setMovieName("TestMovieName1");
		expectedMovie.setRented(true);
		expectedMovie.setYearOfRelease(2016);
		expectedMovie.setRentalAmount(2500F);
		movieList.add(expectedMovie);
		//RegisteredUser
		when(searchMoviePoratlDao.selectMoviePortalUser(moviePortalUser.getEmailAddress())).thenReturn("Registered");
		registeredUserMoviePortalDao.rentMovie(moviePortalUser);
		when(searchMoviePoratlDao.searchMovieByGenere("TestGenere1")).thenReturn(movieList);
		List<Movie> found = onlineMoviePortalService.getMovieListByGenre("TestGenere1");
		assertNotNull(found);
		assertEquals(movieList, found);
		//RegularUser
		Movie expectedMovie2 = new Movie();
		expectedMovie2.setGenere("TestGenere2");
		expectedMovie2.setMovieId(4);
		expectedMovie2.setMovieName("TestMovieName2");
		expectedMovie2.setRented(true);
		expectedMovie2.setYearOfRelease(2016);
		expectedMovie2.setRentalAmount(2500F);
		movieList.add(expectedMovie2);
		moviePortalUser.setMovieList(movieList);
		when(searchMoviePoratlDao.selectMoviePortalUser(moviePortalUser.getEmailAddress())).thenReturn("Regular");
		regularUserMoviePortalDao.rentMovie(moviePortalUser);
		when(searchMoviePoratlDao.searchMovieByGenere("TestGenere1")).thenReturn(movieList);
		List<Movie> foundRegualr = onlineMoviePortalService.getMovieListByGenre("TestGenere1");
		assertNotNull(foundRegualr);
		assertEquals(movieList, foundRegualr);
	}

}
