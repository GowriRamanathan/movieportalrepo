package com.movie.exceptionhandler;

public class NoMovieFoundException extends RuntimeException{
	private static final long serialVersionUID = 1L;
	public NoMovieFoundException(String key){
		super(key+" movies are not available. Please try with another year");
	}
} 
