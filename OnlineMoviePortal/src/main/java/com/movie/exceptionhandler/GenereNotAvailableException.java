package com.movie.exceptionhandler;

public class GenereNotAvailableException extends RuntimeException{
	private static final long serialVersionUID = 1L;
	public GenereNotAvailableException(String key){
		super(key+" not available. Please try with another genere");
	}
} 
