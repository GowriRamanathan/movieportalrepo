package com.movie.exceptionhandler;

public class YearValidationException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	public YearValidationException(String key){
		super(key+" incorrect");
	}
} 
