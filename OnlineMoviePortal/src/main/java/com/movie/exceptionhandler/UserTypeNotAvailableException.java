package com.movie.exceptionhandler;

public class UserTypeNotAvailableException extends RuntimeException{
	private static final long serialVersionUID = 1L;
	public UserTypeNotAvailableException(String key){
		super(key+" not available. emailAddress not matching");
	}
} 
