package com.movie.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.movie.domain.ErrorDetail;
import com.movie.exceptionhandler.GenereNotAvailableException;
import com.movie.exceptionhandler.YearValidationException;

@Component
public class ExceptionHandlingController {

	@ExceptionHandler(value = { YearValidationException.class, GenereNotAvailableException.class })
	public ErrorDetail myError(HttpServletRequest request, Exception exception) {
		ErrorDetail error = new ErrorDetail();
		error.setStatus(HttpStatus.BAD_REQUEST.value());
		error.setMessage(exception.getLocalizedMessage());
		error.setUrl(request.getRequestURL().toString());
		return error;
	}
}
