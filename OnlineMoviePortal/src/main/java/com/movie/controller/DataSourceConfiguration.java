package com.movie.controller;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.movie.dao.IMoviePortalDao1;
import com.movie.dao.RegisteredUserMoviePortalDao;
import com.movie.dao.RegularUserMoviePortalDao;
 
@Configuration
@ComponentScan(basePackages={"com.movie.dao","com.movie.service"})
@EnableWebMvc
public class DataSourceConfiguration extends WebMvcConfigurerAdapter{
	
	@Value("${spring.datasource.driverName}")
	String driverName;
	
	@Value("${spring.datasource.url}")
	String url;
	
	@Value("${spring.datasource.userName}")
	String userName;
	
	@Value("${spring.datasource.password}")
	String password;
 
    @Bean
    public DataSource getDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(driverName);
        dataSource.setUrl(url);
        dataSource.setUsername(userName);
        dataSource.setPassword(password);
         
        return dataSource;
    }
     
    @Bean
    public IMoviePortalDao1 getRegisteredMovieUserDAO() {
        return new RegisteredUserMoviePortalDao(getDataSource());
    }
    
    @Bean
    public IMoviePortalDao1 getRegularMovieUserDAO() {
        return new RegularUserMoviePortalDao(getDataSource());
    }
}