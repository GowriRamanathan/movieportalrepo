package com.movie.constants;

public class ServiceConstants {

	public static final int DISCOUNT_RATE = 20;
	public static final int NO_OF_DAYS_REGISTERED_USERS =7;
	public static final int NO_OF_DAYS_REGULAR_USERS = 3;
}
