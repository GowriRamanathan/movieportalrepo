package com.movie.dao;

import com.movie.domain.MoviePortalUser;

public interface IMoviePortalDao1 {
	/**
     * Rent a movie for a particular user
     *
     * @param MoviePortalUser user to whom the movie has to be rented
     */
	public void rentMovie(MoviePortalUser user);
	
}
