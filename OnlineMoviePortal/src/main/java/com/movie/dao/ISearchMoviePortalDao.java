package com.movie.dao;

import java.util.List;

import com.movie.domain.Movie;

public interface ISearchMoviePortalDao {
	/**
     * Search Movie List based on Genre
     *
     * @param searchString the type of genre whose associated movies has to be searched
     * @return the list of matching movies
     */
	public List<Movie> searchMovieByGenere(String searchString);
	
	/**
     * Search Movie List based on year of release
     *
     * @param searchString the year of release whose associated movies has to be searched
     * @eturn the list of matching movies
     */
	public List<Movie> searchMovieByYear(String searchString);
	
	/**
     * Retrieve list of rented movies
     
     * @return the list of rented movies
     */
	public List<Movie> getRentedMovieList();
	
	/**
     * Retrieve user type for a given user based on email address
     *
     * @param emailAddress the emailAddress of the user
     * @return the user type of matching emailAddress
     */
	public String selectMoviePortalUser(String emailAddress);
}
