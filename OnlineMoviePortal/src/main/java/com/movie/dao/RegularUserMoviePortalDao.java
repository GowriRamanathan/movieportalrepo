
package com.movie.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.movie.constants.DBConstants;
import com.movie.constants.ServiceConstants;
import com.movie.domain.Movie;
import com.movie.domain.MoviePortalUser;

@Repository
@Qualifier("Regular")
public class RegularUserMoviePortalDao implements IMoviePortalDao1 {
	JdbcTemplate jdbcTemplate;	
	
	public RegularUserMoviePortalDao(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

	@Transactional
	public void rentMovie(MoviePortalUser user) {
		updateMovie(user.getMovieList());
		insertUserMovieMapping(user.userId, user.getMovieList());
	}

	public void updateMovie(final List<Movie> movieList) {
		List<Integer> inputList = new ArrayList<Integer>();
		for (Movie m : movieList) {
			inputList.add(m.getMovieId());
		}
		final LocalDate date = LocalDate.now().plus(ServiceConstants.NO_OF_DAYS_REGULAR_USERS, ChronoUnit.DAYS);
		jdbcTemplate.batchUpdate(DBConstants.UPDATE_MOVIE_USER_SQL, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Movie m = movieList.get(i);
				ps.setInt(1, m.getMovieId());
				ps.setDate(2, java.sql.Date.valueOf(date));
				ps.setFloat(3, m.getRentalAmount());
			}
			public int getBatchSize() {
				return movieList.size();
			}
		});
	}

	public void insertUserMovieMapping(final int userId, final List<Movie> movieList) {
		jdbcTemplate.batchUpdate(DBConstants.INSERT_USER_MOVIE_MAPING_SQL, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ps.setInt(1, userId);
				Movie m = movieList.get(i);
				ps.setInt(2, m.getMovieId());
			}
			public int getBatchSize() {
				return movieList.size();
			}
		});
	}

}
