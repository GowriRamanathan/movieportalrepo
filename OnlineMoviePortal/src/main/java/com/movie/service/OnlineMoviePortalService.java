package com.movie.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.movie.dao.IMoviePortalDao1;
import com.movie.dao.ISearchMoviePortalDao;
import com.movie.domain.Movie;
import com.movie.domain.MoviePortalUser;
import com.movie.enums.UserType;
import com.movie.exceptionhandler.UserTypeNotAvailableException;

@Service
public class OnlineMoviePortalService {
	
	
	@Autowired
	@Qualifier("registeredUserMoviePortalDao")
	IMoviePortalDao1 registeredUserMoviePortalDao;
	
	@Autowired
	@Qualifier("regularUserMoviePortalDao")
	IMoviePortalDao1 regularUserMoviePortalDao;
	
	@Autowired
	ISearchMoviePortalDao iSearchMoviePortalDao;
	
	public IMoviePortalDao1 getRegisteredUserMoviePortalDao() {
		return registeredUserMoviePortalDao;
	}

	public void setRegisteredUserMoviePortalDao(IMoviePortalDao1 registeredUserMoviePortalDao) {
		this.registeredUserMoviePortalDao = registeredUserMoviePortalDao;
	}

	public IMoviePortalDao1 getRegularUserMoviePortalDao() {
		return regularUserMoviePortalDao;
	}

	public void setRegularUserMoviePortalDao(IMoviePortalDao1 regularUserMoviePortalDao) {
		this.regularUserMoviePortalDao = regularUserMoviePortalDao;
	}

	public ISearchMoviePortalDao getiSearchMoviePortalDao() {
		return iSearchMoviePortalDao;
	}

	public void setiSearchMoviePortalDao(ISearchMoviePortalDao iSearchMoviePortalDao) {
		this.iSearchMoviePortalDao = iSearchMoviePortalDao;
	}

	public List<Movie> getMovieListByGenre(String genre) {
		List<Movie> movieList = new ArrayList<Movie>();
		if (null != genre) {
			movieList = iSearchMoviePortalDao.searchMovieByGenere(genre);
		} 
		return movieList;
	}
	
	public List<Movie> getMovieListByYear(String year) {
		List<Movie> movieList = new ArrayList<Movie>();
		if (null != year) {
			movieList = iSearchMoviePortalDao.searchMovieByYear(year);
		} 
		return movieList;
	} 
	
	public void rentMovie(MoviePortalUser user){
		String userType = iSearchMoviePortalDao.selectMoviePortalUser(user.getEmailAddress());
		if(null == userType){
			throw new UserTypeNotAvailableException(userType);
		}
		if(userType.equals(UserType.REGISTERED.name())){
			registeredUserMoviePortalDao.rentMovie(user);
		}else if (userType.equals(UserType.REGISTERED.name())){
			regularUserMoviePortalDao.rentMovie(user);
		}		
	}
	
	public List<Movie> getRentedMoviesList(){
		List<Movie> movieList = iSearchMoviePortalDao.getRentedMovieList();
		return movieList;
	}

}
