package com.movie.domain;

import java.util.Date;

public class Movie {
	public String movieName;
	public int movieId;
	public boolean isRented;
	public Date expiryDate;
	public Float rentalAmount;
	public String genere;
	public int yearOfRelease;
	
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public int getMovieId() {
		return movieId;
	}
	public void setMovieId(int movieId) {
		this.movieId = movieId;
	}
	public boolean isRented() {
		return isRented;
	}
	public void setRented(boolean isRented) {
		this.isRented = isRented;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Float getRentalAmount() {
		return rentalAmount;
	}
	public void setRentalAmount(Float rentalAmount) {
		this.rentalAmount = rentalAmount;
	}
	public String getGenere() {
		return genere;
	}
	public void setGenere(String genere) {
		this.genere = genere;
	}
	public int getYearOfRelease() {
		return yearOfRelease;
	}
	public void setYearOfRelease(int yearOfRelease) {
		this.yearOfRelease = yearOfRelease;
	}
	
}
